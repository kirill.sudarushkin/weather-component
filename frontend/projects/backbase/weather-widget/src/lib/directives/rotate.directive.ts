import { Directive, ElementRef, Input, OnChanges } from '@angular/core';
import { SmartChanges } from '../utils/change-detector.utils';

@Directive({
    selector: '[libRotate]'
})
export class RotateDirective implements OnChanges {

    @Input()
    public libRotate: number = 0;

    constructor(private elementRef: ElementRef) {
    }

    public ngOnChanges(changes: SmartChanges<this>): void {
        if (changes.libRotate) {
            const transform: string = this.elementRef.nativeElement.style.transform
                .replace(/rotate\(\d+deg\)/, '');

            this.elementRef.nativeElement.style.transform = `${transform} rotate(${changes.libRotate.currentValue ?? 0}deg)`;
        }
    }
}
