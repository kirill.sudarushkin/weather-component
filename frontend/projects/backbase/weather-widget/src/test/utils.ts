import { waitForAsync as ngWaitForAsync } from '@angular/core/testing';

export  function waitForAsync(func: () => Promise<void>): (done: any) => any {
    // @ts-ignore
    return ngWaitForAsync(() => func().catch(fail));
}
