import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HourSummaryComponent } from './hour-summary.component';
import { KelvinToCelsiusPipe } from '../../pipes/kelvin-to-celsius.pipe';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('HourSummaryComponent', () => {
  let component: HourSummaryComponent;
  let fixture: ComponentFixture<HourSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HourSummaryComponent, KelvinToCelsiusPipe ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HourSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
