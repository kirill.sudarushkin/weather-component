import { AfterViewChecked, ChangeDetectionStrategy, Component, ElementRef, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { CityForecast, WeatherWidgetStore } from '../../services/weather-widget.store';
import { CityNotFoundError } from '../../models/city-not-found.error';
import '@angular/localize/init';

@Component({
    selector: 'bb-weather-widget',
    templateUrl: 'weather-widget.component.html',
    styleUrls: ['weather-widget.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [WeatherWidgetStore],
})
export class WeatherWidgetComponent implements AfterViewChecked {

    @ViewChild('queryInput', {static: false})
    private queryInput?: ElementRef;

    public readonly error$: Observable<any>;
    public readonly loading$: Observable<boolean> = this.store.loading$;
    public readonly forecasts$: Observable<CityForecast[]>;

    public query: string = '';
    public queryFormVisible: boolean = false;

    constructor(
        private readonly store: WeatherWidgetStore
    ) {
        this.forecasts$ = this.store.cities$.pipe(
            map(cities => [...cities]
                .sort((a, b) => a.order - b.order))
        );

        this.error$ = this.store.error$.pipe(
            filter(Boolean),
            tap(console.error),
            map(x => x instanceof CityNotFoundError
                ? $localize`:Weather widget|City not found error:Couldn't find city for query "${x.query}".`
                : $localize`:Weather widget|Unexpected error when loading weather data:Unexpected error when loading weather data.`
            )
        );
    }

    public ngAfterViewChecked(): void {
        this.queryInput?.nativeElement.focus();
    }

    public summaryId(index: number, item: CityForecast): number {
        return item.city.id;
    }

    public addCity(query: string): void {
        this.store.addCity({query});
        this.query = '';
    }
}
