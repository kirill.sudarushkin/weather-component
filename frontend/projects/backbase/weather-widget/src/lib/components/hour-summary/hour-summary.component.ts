import { Component, Input } from '@angular/core';
import { WeatherSummary } from '../../models/weather-summary';

@Component({
    selector: 'lib-hour-summary',
    templateUrl: './hour-summary.component.html',
    styleUrls: ['./hour-summary.component.scss']
})
export class HourSummaryComponent {

    @Input()
    public summary?: WeatherSummary;

}
