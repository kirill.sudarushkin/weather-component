# Weather widget

Hi there! It's an Angular component displaying weather forecasts in selected cities.
You could try it alive here: https://kirillsud.gitlab.io/weather-component!

Component uses https://openweathermap.org/ API for weather forecasts.

Component's features:

* Displays current weather for a city
* Displays hourly forecast for a city
* Allows modifying cities list
* Stores data in LocalStorage
* Refreshes data by timeout
* Supports color theming by CSS variables
* Supports Angular internationalization (i18n)

## Manual

### Install

Register module registry namespace locally. For example, by creating a local `.npmrc` file with the following line:

```
@kirillsud:registry=https://gitlab.com/api/v4/packages/npm/
```

Then, install the module, and it's dependencies, if needed:

```bash
npm install --save @kirillsud/weather-component @ngrx/component-store@11
```

After that, you should import `WeatherWidgetModule` into your application module:

```typescript
import { NgModule, Component } from '@angular/core';
import { WeatherWidgetModule } from '@backbase/weather-widget';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        WeatherWidgetModule.forRoot({
            openWeatherApiToken: 'xxx',
            refreshTimeout: 5 * 60 * 1000
        })
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
```

Now you can include weather component in your Angular template;

```html
<bb-weather-widget></bb-weather-widget>
```

### Configuration

WeatherWidgetModule could be configured with the following options:

* `openWeatherApiToken` (required) - API token from https://openweathermap.org
* `openWeatherApiUrl` (optional) - URL to OpenWeatherMap API endpoint; could be using for proxy
* `refreshTimeout` (optional) - timeout in milliseconds to update forecasts; default is 5 minutes

## Development

There are two folders in the root: **middleware** and **frontend**.

### Middleware

First one is a hypothetical server app providing security for a OpenWeather API token
and possibility to manage traffic to API. You could find here only one file 
with the solution idea for that.

### Frontend

The second one contains the Angular application, implementing npm library with 
the weather widget and a demo app with some documentation for it. It could be used for
testing and development purposes.

All further command should be executed inside the **frontend** folder.

#### Development server

First, run `npm run lib:watch` to start library watcher.

Then, run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app 
will automatically reload if you change any of the source files.

#### Build

Run `npm run lib:build` to build the library package.

Run `npm run build` to build the demo app.

The build artifacts will be stored in the `dist/` directory.

#### Running unit tests

Run `npm run lib:test` to execute the library unit tests via [Jest](http://jest.io/).

#### Running typescript lint

Run `npm run lib:lint` to execute the typescript checks vis [TSLint](https://palantir.github.io/tslint/).
