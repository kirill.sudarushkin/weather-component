import { InjectionToken } from '@angular/core';

export const weatherWidgetConfigToken = new InjectionToken<WeatherWidgetConfig>('WeatherWidgetConfig');

export interface WeatherWidgetConfig {
    openWeatherApiUrl?: string;
    openWeatherApiToken: string;
    refreshTimeout: number;
}
