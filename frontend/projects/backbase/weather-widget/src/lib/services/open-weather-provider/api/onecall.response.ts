export interface WeatherData {
    // Time of data calculation, unix, UTC
    dt: number;
    // Sunrise time, Unix, UTC
    sunrise: number;
    // Sunset time, Unix, UTC
    sunset: number;
    // Temperature.
    // Units - default: kelvin, metric: Celsius, imperial: Fahrenheit.
    temp: number;
    // Temperature. This temperature parameter accounts for the human perception of weather.
    // Units – default: kelvin, metric: Celsius, imperial: Fahrenheit.
    feels_like: number;
    // Atmospheric pressure on the sea level, hPa
    pressure: number;
    // Humidity, %
    humidity: number;
    // Atmospheric temperature (varying according to pressure and humidity) below which
    // water droplets begin to condense and dew can form.
    // Units – default: kelvin, metric: Celsius, imperial: Fahrenheit.
    dew_point: number;
    // Cloudiness, %
    clouds: number;
    // Current UV index
    uvi: number;
    // Average visibility, metres
    visibility: number;
    // Wind speed. Wind speed.
    // Units – default: metre/sec, metric: metre/sec, imperial: miles/hour.
    wind_speed: number;
    // (where available) Wind gust.
    // Units – default: metre/sec, metric: metre/sec, imperial: miles/hour.
    wind_gust: number;
    // Wind direction, degrees (meteorological)
    wind_deg: number;
    rain: {
        // (where available) Rain volume for last hour, mm
        '1h': number;
    };
    snow: {
        // (where available) Snow volume for last hour, mm
        '1h': number;
    };
    weather: {
        // Weather condition id
        id: number;
        // Group of weather parameters (Rain, Snow, Extreme etc.)
        main: string;
        // Weather condition within the group.
        description: string;
        // Weather icon id
        icon: string;
    }[];
}

export interface HourlyWeatherData extends WeatherData {
    // Probability of precipitation
    pop: number;
}

export interface OnecallResponse {
    // Geographical coordinates of the location (latitude)
    lon: number;
    // Geographical coordinates of the location (longitude)
    lat: number;
    // Timezone name for the requested location
    timezone: string;
    // Shift in seconds from UTC
    timezone_offset: number;
    // Current weather data API response
    current?: WeatherData;
    // Hourly forecast weather data API response
    hourly?: HourlyWeatherData[];
}
