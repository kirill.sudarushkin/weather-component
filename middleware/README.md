# Middleware

Here should be designed and implemented solution to protect
OpenWeatherMap API Token from public access.

The easiest way - using any configurable server daemon, e.g. Nginx.
It could be configured as a proxy with adding API token for each request. 
