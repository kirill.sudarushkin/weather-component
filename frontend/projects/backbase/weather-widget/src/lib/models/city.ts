export class City {
    public readonly id: number;

    /**
     * City name
     */
    public readonly name: string;

    /**
     * City geo location, latitude
     */
    public readonly latitude: number;

    /**
     * City geo location, longitude
     */
    public readonly longitude: number;

    /**
     * Angular locale code for city
     */
    public readonly locale: string;

    constructor(id: number, name: string, latitude: number, longitude: number, locale: string) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.locale = locale;
    }
}
