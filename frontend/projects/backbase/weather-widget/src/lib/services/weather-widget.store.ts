import { EMPTY, from, interval, Observable, Subscription } from 'rxjs';
import {
    catchError,
    distinctUntilChanged,
    filter,
    finalize,
    map,
    mapTo,
    mergeMap,
    skip,
    startWith,
    switchMap,
    take,
    takeUntil,
    tap
} from 'rxjs/operators';
import { Inject, Injectable, OnDestroy } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { City } from '../models/city';
import { WeatherSummary } from '../models/weather-summary';
import { WeatherProvider } from './weather.provider';
import { WeatherWidgetConfig, weatherWidgetConfigToken } from '../weather-widget.config';

export type CityForecast = {
    city: City;
    loading: boolean;
    forecast?: WeatherSummary[];
    error?: any;
    order: number;
};

export interface QueryWithOrder {
    query: string;
    order?: number;
}

export interface WeatherWidgetState {
    cities: CityForecast[];
    loading: boolean;
    error: any;
}

const localStorageKey = 'weather-widget.cities';

@Injectable()
export class WeatherWidgetStore extends ComponentStore<WeatherWidgetState> implements OnDestroy {

    private saveSubscriber: Subscription;

    constructor(
        private readonly weatherProvider: WeatherProvider,
        @Inject(weatherWidgetConfigToken)
        private readonly config: WeatherWidgetConfig,
    ) {
        super({cities: [], loading: false, error: undefined});

        const loadingResult = this.loadFromLocalStorage();

        // Add default cities if LS is empty
        if (!loadingResult) {
            const cityNames = ['Saint Petersburg', 'Riga', 'Gdansk', 'Leeuwarden', 'Hamburg'];
            cityNames.forEach((x, i) => this.addCity({query: x, order: i}));
        }

        this.saveSubscriber = this.cities$
            .pipe(skip(1)) // Skip first emission with default store state
            .subscribe(cities => this.saveToLocalStorage(cities));
    }

    // *********** Updaters *********** //

    public readonly addCity = this.effect((query$: Observable<QueryWithOrder>) => {
        return query$.pipe(
            mergeMap(({query, order = 0}) => {
                this.setLoading(true);

                return from(this.weatherProvider.searchCity(query)).pipe(
                    tap({
                        next: (city) => this.addForecast({city, order, loading: true}),
                        error: (error) => this.setError(error)
                    }),
                    finalize(() => this.setLoading(false)),
                    catchError(() => EMPTY),
                    switchMap(city => this.startForecastStream(city))
                );
            })
        );
    });

    public readonly removeCity = this.updater((state: WeatherWidgetState, city: City) => {
        const cities = this.removeFromList(state, city);

        this.saveToLocalStorage(cities);

        return {...state, cities};
    });

    private readonly addForecast = this.updater((state: WeatherWidgetState, forecast: CityForecast) => {
        const existing = state.cities.find(x => x.city.id === forecast.city.id);

        if (existing) {
            throw new Error(`City "${forecast.city.name}" already exists in the store.`);
        }

        // Let's shift all forecasts orders that greater then new one
        let currentOrder = forecast.order;
        const cities = state.cities
            .sort((a, b) => a.order - b.order)
            .map(x => {
                if (x.order !== currentOrder) {
                    return x;
                }

                currentOrder++;
                return {...x, order: x.order + 1};
            });

        return {
            ...state,
            cities: [
                ...cities,
                forecast
            ]
        };
    });

    private readonly updateForecast = this.updater((state: WeatherWidgetState, forecast: Omit<CityForecast, 'order'>) => {
        const existing = state.cities.find(x => x.city.id === forecast.city.id);

        if (!existing) {
            throw new Error(`City "${forecast.city.name}" doesn't exist in the store.`);
        }

        return {
            ...state,
            cities: [
                ...this.removeFromList(state, forecast.city),
                {...forecast, order: existing.order}
            ]
        };
    });

    private readonly setLoading = this.updater((state: WeatherWidgetState, loading: boolean) => {
        return {
            ...state,
            loading,
            error: undefined
        };
    });

    private readonly setError = this.updater((state: WeatherWidgetState, error: any) => {
        return {
            ...state,
            error
        };
    });

    // *********** Selectors *********** //

    public readonly cities$ = this.select(x => x.cities);
    public readonly error$ = this.select(x => x.error);
    public readonly loading$ = this.select(x => x.loading);

    public onRemoved(city: City): Observable<boolean> {
        return this.select(state => state.cities).pipe(
            map(x => x.every(y => y.city.id !== city.id)),
            distinctUntilChanged(),
            filter(x => x === true),
            take(1)
        );
    }

    // *********** Other implementation *********** //

    public ngOnDestroy(): void {
        this.saveSubscriber.unsubscribe();
        super.ngOnDestroy();
    }

    private startForecastStream(city: City): Observable<void> {
        // Disable weather refresh for all timeouts below 10 sec.
        const interval$ = this.config.refreshTimeout < 10000
            ? EMPTY
            : interval(this.config.refreshTimeout);

        return interval$.pipe(
            startWith(void 0),
            switchMap(() => this.weatherProvider.getHourlyForecastByCity(city)),
            tap({
                next: (forecast) => this.updateForecast({city, loading: false, forecast}),
                error: (error) => this.updateForecast({city, loading: false, error})
            }),
            catchError(() => EMPTY),
            takeUntil(this.onRemoved(city)),
            mapTo(undefined)
        );
    }

    private removeFromList(state: WeatherWidgetState, city: City): CityForecast[] {
        return state.cities.filter(x => x.city.id !== city.id);
    }

    private loadFromLocalStorage(): boolean {
        const item = localStorage.getItem(localStorageKey);
        if (!item) {
            return false;
        }

        let cityNames: QueryWithOrder[];
        try {
            cityNames = JSON.parse(item);
        } catch (e) {
            return false;
        }

        if (!Array.isArray(cityNames)) {
            return false;
        }

        cityNames.forEach(x => this.addCity(x));

        return true;
    }

    private saveToLocalStorage(cities: CityForecast[]): void {
        const queries: QueryWithOrder[] = cities.map(x => ({
            query: x.city.name,
            order: x.order
        }));

        const value: string = JSON.stringify(queries);
        localStorage.setItem(localStorageKey, value);
    }
}
