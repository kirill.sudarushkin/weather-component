import { Component, Inject, LOCALE_ID } from '@angular/core';
import { DOCUMENT } from '@angular/common';

const themeLsKey = 'style-theme';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    public language: string;

    private themeValue: string = '';

    constructor(
        @Inject(DOCUMENT)
        private readonly document: Document,
        @Inject(LOCALE_ID)
        private readonly localeId: string
    ) {
        this.theme = localStorage.getItem(themeLsKey) ?? 'auto';
        this.language = this.localeId.substr(0, 2);
    }

    public get theme(): string {
        return this.themeValue;
    }

    public set theme(name: string) {
        let className: string = `${name}-theme`;

        if (name === 'auto') {
            const hours = new Date().getHours();
            const darkEnabled = hours >= 20 || hours < 5;
            className = darkEnabled ? 'dark-theme' : 'light-theme';
        }

        document.body.classList.forEach(x => {
            if (!x.endsWith('-theme') || x === className) {
                return;
            }

            document.body.classList.remove(x);
        });

        document.body.classList.add(className);
        localStorage.setItem(themeLsKey, name);

        this.themeValue = name;
    }
}
