import { City } from './city';
import { Kelvin, MeteorologicalDegrees, MetersInSec } from './number-guards';

export abstract class WeatherSummary {
    /**
     * Location of the weather summary
     */
    public readonly city: City;

    /**
     * Temperature in Kelvin
     */
    public abstract readonly temperature: Kelvin;

    /**
     * Wind speed in meters/sec
     */
    public abstract readonly windSpeed: MetersInSec;

    /**
     * Wind direction in degrees
     */
    public abstract readonly windDirection: MeteorologicalDegrees;

    /**
     * Time of data calculation
     */
    public abstract readonly date: Date;

    /**
     * Text description of current weather summary
     */
    public abstract readonly description: string;

    /**
     * URL to an icon that represents current weather summary
     */
    public abstract readonly iconUrl: string;

    protected constructor(city: City) {
        this.city = city;
    }
}
