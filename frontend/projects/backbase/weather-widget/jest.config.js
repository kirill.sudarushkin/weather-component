module.exports = {
    preset: 'jest-preset-angular',
    setupFilesAfterEnv: ['<rootDir>/projects/backbase/weather-widget/jest.setup.ts'],
    transform: {"\\.ts$": ['ts-jest']},
    coverageDirectory: 'coverage/backbase/weather-widget',
    coverageReporters: ['text-summary', 'cobertura'],
    reporters: [
        "default",
        [
            "jest-junit",
            {
                outputDirectory: '<rootDir>/projects/backbase/weather-widget',
                // Some improvement for GitLab test reports output:
                classNameTemplate: vars => vars.suitename,
                titleTemplate: vars => (vars.classname.replace(vars.suitename, '') + ' ' + vars.title).trim()
            }
        ]
    ]
}
