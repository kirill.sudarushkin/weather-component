export const msInSecond = 1000;
export const msInMinute = 60 * msInSecond;
export const msInHour = 60 * msInMinute;
