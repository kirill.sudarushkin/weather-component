import { SimpleChange, SimpleChanges } from '@angular/core';

export interface SmartChange<T> extends SimpleChange {
    previousValue: T | undefined;
    currentValue: T | undefined;
}

export type SmartChanges<T> = SimpleChanges & {
    [Key in keyof T]?: SmartChange<T[Key]>;
};
