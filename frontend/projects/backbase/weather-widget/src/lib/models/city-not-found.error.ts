export class CityNotFoundError extends Error {
    constructor(
        public readonly query: string
    ) {
        super(`Couldn't find city for query "${query}".`);
    }
}
