import { ModuleWithProviders, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { WeatherProvider } from './services/weather.provider';
import { WeatherWidgetComponent } from './components/weather-widget/weather-widget.component';
import { OpenWeatherProvider } from './services/open-weather-provider/open-weather.provider';
import { WeatherWidgetConfig, weatherWidgetConfigToken } from './weather-widget.config';
import { CitySummaryComponent } from './components/city-summary/city-summary.component';
import { KelvinToCelsiusPipe } from './pipes/kelvin-to-celsius.pipe';
import { RotateDirective } from './directives/rotate.directive';
import { HourSummaryComponent } from './components/hour-summary/hour-summary.component';
import { msInMinute } from './utils/date.utils';
import { WindDirectionComponent } from './components/wind-direction/wind-direction.component';

export type WeatherWidgetModuleConfig = WeatherWidgetConfig | Omit<WeatherWidgetConfig, 'refreshTimeout'>;

@NgModule({
    declarations: [
        WeatherWidgetComponent,
        CitySummaryComponent,
        KelvinToCelsiusPipe,
        RotateDirective,
        HourSummaryComponent,
        WindDirectionComponent
    ],
    imports: [
        HttpClientModule,
        CommonModule,
        FormsModule,
        BrowserModule,
        BrowserAnimationsModule
    ],
    providers: [
        {
            provide: WeatherProvider,
            useClass: OpenWeatherProvider
        }
    ],
    exports: [WeatherWidgetComponent]
})
export class WeatherWidgetModule {
    public static forRoot(config: WeatherWidgetModuleConfig): ModuleWithProviders<WeatherWidgetModule> {
        return {
            ngModule: WeatherWidgetModule,
            providers: [
                {
                    provide: weatherWidgetConfigToken,
                    useValue: {
                        refreshTimeout: 5 * msInMinute,
                        ...config
                    } as WeatherWidgetConfig
                }
            ]
        };
    }
}
