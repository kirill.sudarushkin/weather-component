import { ComponentFixture, TestBed } from '@angular/core/testing';

import { minWindArrowFontSize, WindDirectionComponent } from './wind-direction.component';
import { MeteorologicalDegrees, MetersInSec } from '../../models/number-guards';
import { RotateDirective } from '../../directives/rotate.directive';

describe('WindDirectionComponent', () => {
    let component: WindDirectionComponent;
    let fixture: ComponentFixture<WindDirectionComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [WindDirectionComponent, RotateDirective]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(WindDirectionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('ngOnChanges should calculate wind arrow size', () => {
        component.speed = 6 as MetersInSec;
        component.deg = 100 as MeteorologicalDegrees;

        expect(component.arrowSize).toBe(minWindArrowFontSize);

        component.ngOnChanges();

        expect(component.arrowSize).toBeCloseTo(1.09725);
    });
});
