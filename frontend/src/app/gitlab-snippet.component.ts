import { AfterViewInit, Component, ElementRef, Inject, Input, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-gitlab-snippet',
    template: '<iframe #iframe></iframe>',
    styles: [`
        :host {
            display: block;
        }

        :host iframe {
            width: 100%;
            border: 0;
            /* clearing gitlab inner big margins */
            margin: -20px;
        }
    `]
})
export class GitlabSnippetComponent implements AfterViewInit {

    @Input()
    public project?: string;

    @Input()
    public id?: number | string;

    @ViewChild('iframe', {static: true})
    public iframe?: ElementRef;

    constructor(
        private readonly elementRef: ElementRef,
        private readonly http: HttpClient,
        @Inject(DOCUMENT)
        private readonly document: Document
    ) {
    }

    public ngAfterViewInit(): void {
        const url: string = `//gitlab.com/${this.project}/-/snippets/${this.id}.js`;

        const iframe: HTMLIFrameElement = this.iframe?.nativeElement;

        if (iframe?.contentWindow) {
            const document: Document = iframe.contentWindow.document;
            document.open();
            document.write(`<script src="${url}"></script>`);
            document.close();

            iframe.contentWindow.addEventListener('load', () => {
                iframe.style.height = document.body.scrollHeight.toString(10) + 'px';
            });
        }
    }
}
