import { KelvinToCelsiusPipe } from './kelvin-to-celsius.pipe';
import { Kelvin } from '../models/number-guards';

describe('KelvinToCelsiusPipe', () => {
    it('create an instance', () => {
        const pipe = new KelvinToCelsiusPipe();
        expect(pipe).toBeTruthy();
    });

    it('should convert 273.15°K to 0°C', () => {
        const pipe = new KelvinToCelsiusPipe();
        expect(pipe.transform(273.15 as Kelvin)).toBe(0);
    });

    it('should convert 0°K to -273.15°C', () => {
        const pipe = new KelvinToCelsiusPipe();
        expect(pipe.transform(273.15 as Kelvin)).toBe(0);
    });

    it('should convert 300.30°K to 27.15°C', () => {
        const pipe = new KelvinToCelsiusPipe();
        expect(pipe.transform(273.15 as Kelvin)).toBe(0);
    });
});
