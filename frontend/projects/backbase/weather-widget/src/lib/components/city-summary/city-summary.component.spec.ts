import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange } from '@angular/core';

import { CitySummaryComponent } from './city-summary.component';
import { CityForecast, WeatherWidgetStore } from '../../services/weather-widget.store';
import { WeatherSummary } from '../../models/weather-summary';
import { msInHour, msInMinute } from '../../utils/date.utils';
import { KelvinToCelsiusPipe } from '../../pipes/kelvin-to-celsius.pipe';

describe('CitySummaryComponent', () => {
    let component: CitySummaryComponent;
    let fixture: ComponentFixture<CitySummaryComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [CitySummaryComponent, KelvinToCelsiusPipe],
            providers: [
                {
                    provide: WeatherWidgetStore,
                    useValue: {
                        removeCity: jest.fn()
                    } as Partial<WeatherWidgetStore>
                }
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CitySummaryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('ngOnChanges should initialize current', () => {
        const now: number = Date.now();

        component.forecast = {
            forecast: [
                {date: new Date(now - msInMinute)} as WeatherSummary,
                {date: new Date(now + msInHour)} as WeatherSummary,
                {date: new Date(now + msInHour * 2)} as WeatherSummary,
            ]
        } as CityForecast;

        expect(component.current).toBeUndefined();
        expect(component.hourly).toBeUndefined();

        component.ngOnChanges({
            forecast: new SimpleChange(undefined, component.forecast, false)
        });

        expect(component.current).toBeDefined();
        expect(component.current?.date.valueOf()).toBeLessThanOrEqual(now);
    });

    it('ngOnChanges should initialize hourly', () => {
        const now: number = Date.now();

        component.forecast = {
            forecast: [
                {date: new Date(now - msInMinute)} as WeatherSummary,
                {date: new Date(now + msInHour)} as WeatherSummary,
                {date: new Date(now + msInHour * 2)} as WeatherSummary,
            ]
        } as CityForecast;

        expect(component.current).toBeUndefined();
        expect(component.hourly).toBeUndefined();

        component.ngOnChanges({
            forecast: new SimpleChange(undefined, component.forecast, false)
        });

        expect(component.hourly).toBeDefined();
        expect(component.hourly?.length).toBe(2);
        expect(component.hourly?.[0].date.valueOf()).toBeGreaterThan(now);
        expect(component.hourly?.[1].date.valueOf()).toBeGreaterThan(now);
    });
});
