export type Kelvin = number & { type: 'kelvin' };
export type Celsius = number & { type: 'celsius' };

export type MetersInSec = number & { type: 'meters/sec' };

/**
 * Meteorological degrees - starting from north clockwise
 */
export type MeteorologicalDegrees = number & { type: 'meteorological degrees' };
