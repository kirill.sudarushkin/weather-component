import { Component, Input, OnChanges } from '@angular/core';
import { MeteorologicalDegrees, MetersInSec } from '../../models/number-guards';

export const minWindArrowFontSize = 0.5;

@Component({
    selector: 'lib-wind-direction',
    templateUrl: './wind-direction.component.html',
    styleUrls: ['./wind-direction.component.scss']
})
export class WindDirectionComponent implements OnChanges {

    @Input()
    public speed?: MetersInSec;

    @Input()
    public deg?: MeteorologicalDegrees;

    public arrowSize: number = minWindArrowFontSize;

    constructor() {
    }

    public ngOnChanges(): void {
        const fontSize: number = Math.log(this.speed ?? 0) / 3 + minWindArrowFontSize;
        this.arrowSize = Math.max(fontSize, minWindArrowFontSize);
    }

}
