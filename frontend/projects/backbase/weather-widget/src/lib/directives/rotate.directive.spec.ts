import { RotateDirective } from './rotate.directive';
import { ElementRef } from '@angular/core';

describe('RotateDirective', () => {
    it('should create an instance', () => {
        const directive = new RotateDirective(new ElementRef(document.createElement('div')));
        expect(directive).toBeTruthy();
    });
});
