import { Injectable } from '@angular/core';
import { WeatherSummary } from '../models/weather-summary';
import { City } from '../models/city';

@Injectable()
export abstract class WeatherProvider {
    /**
     * Searches city by query
     * @throws CityNotFoundException
     */
    public abstract searchCity(query: string): Promise<City>;

    /**
     * Returns current weather for specified city
     */
    public abstract getCurrentWeatherByCity(city: City): Promise<WeatherSummary>;

    /**
     * Returns weather forecast for specified city
     */
    public abstract getHourlyForecastByCity(city: City): Promise<WeatherSummary[]>;
}
