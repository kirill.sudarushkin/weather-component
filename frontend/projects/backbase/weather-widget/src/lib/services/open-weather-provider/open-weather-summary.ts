import { WeatherSummary } from '../../models/weather-summary';
import { City } from '../../models/city';
import { Kelvin, MeteorologicalDegrees, MetersInSec } from '../../models/number-guards';
import { WeatherData } from './api/onecall.response';

export class OpenWeatherSummary extends WeatherSummary {

    public get temperature(): Kelvin {
        return this.data.temp as Kelvin;
    }

    public get windSpeed(): MetersInSec {
        return this.data.wind_speed as MetersInSec;
    }

    public get windDirection(): MeteorologicalDegrees {
        return this.data.wind_deg as MeteorologicalDegrees;
    }

    public get description(): string {
        return this.data.weather[0].description;
    }

    public readonly date: Date;
    public readonly iconUrl: string;

    constructor(
        city: City,
        private readonly data: WeatherData
    ) {
        super(city);

        if (data.weather.length < 1) {
            throw new Error('Expected weather data should be existing.');
        }

        this.date = new Date(data.dt * 1000);
        this.iconUrl = `//openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`;
    }
}
