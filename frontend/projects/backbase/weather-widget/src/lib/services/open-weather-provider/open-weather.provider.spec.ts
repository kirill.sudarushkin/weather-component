import { TestBed } from '@angular/core/testing';
import { LOCALE_ID } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { OpenWeatherProvider } from './open-weather.provider';
import { WeatherWidgetConfig, weatherWidgetConfigToken } from '../../weather-widget.config';
import { WeatherSummary } from '../../models/weather-summary';
import { waitForAsync } from '../../../test/utils';
import { City } from '../../models/city';
import { HourlyWeatherData } from './api/onecall.response';
import { CityNotFoundError } from '../../models/city-not-found.error';
import { OpenWeatherSummary } from './open-weather-summary';

describe('OpenWeatherProvider', () => {
    let service: OpenWeatherProvider;
    let httpController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                {
                    provide: weatherWidgetConfigToken,
                    useValue: {
                        openWeatherApiUrl: '//yyy',
                        openWeatherApiToken: 'xxx'
                    } as WeatherWidgetConfig
                },
                {
                    provide: LOCALE_ID,
                    useValue: 'aa-BB'
                },
                OpenWeatherProvider,
            ]
        });

        service = TestBed.inject(OpenWeatherProvider);
        httpController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        httpController?.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('method searchCity', () => {
        const requestUrl = '//yyy/weather';

        it('should add token to request', () => {
            service.searchCity('foo');

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            expect(req).toBeDefined();

            const url = new URL('https:' + req?.request.url);
            expect(url.searchParams.get('appid')).toBe('xxx');
        });

        it('should add application locale to request', () => {
            service.searchCity('foo');

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            expect(req).toBeDefined();

            const url = new URL('https:' + req?.request.url);
            expect(url.searchParams.get('lang')).toBe('aa');
        });

        it('should add query to request', () => {
            service.searchCity('foo');

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            expect(req).toBeDefined();

            const url = new URL('https:' + req?.request.url);
            expect(url.searchParams.get('q')).toBe('foo');
        });

        it('should escape query special chars', () => {
            service.searchCity('foo&1=2');

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            expect(req).toBeDefined();

            const url = new URL('https:' + req?.request.url);
            expect(url.searchParams.get('q')).toBe('foo&1=2');
        });

        it('should return requested city', waitForAsync(async () => {
            // @ts-ignore
            const data: WeatherResponse = require('../../../test/mocks/open-weather-response-weather.json');

            const expected: City = new City(
                data.id,
                data.name,
                data.coord.lat,
                data.coord.lon,
                'aa-BB'
            );

            const actual$ = service.searchCity('qqq');

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            req.flush(data);

            const actual = await actual$;
            expect(actual).toEqual(expected);
        }));

        it('should throw a CityNotFoundError error for cases when city not found', waitForAsync(async () => {
            const actual$ = service.searchCity('foo');

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            req.error({code: 404, message: 'city not found'} as unknown as ErrorEvent, {
                status: 404
            });

            try {
                await actual$;
                fail('Test should trow an error');
            } catch (e: any) {
                expect(e).toBeInstanceOf(CityNotFoundError);
                expect(e.query).toEqual('foo');
            }
        }));

        it('should throw a HTTP error for any other error in HTTP request', waitForAsync(async () => {
            const actual$ = service.searchCity('foo');

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            req.error('error' as unknown as ErrorEvent);

            try {
                await actual$;
                fail('Test should trow an error');
            } catch (e: any) {
                expect(e).toBeInstanceOf(HttpErrorResponse);
                expect(e.error).toEqual('error');
            }
        }));
    });

    describe('method getCurrentWeatherByCity', () => {
        const requestUrl = '//yyy/onecall';
        let city: City;

        beforeEach(() => {
            city = new City(100, 'foo', 1, 2, 'xxx');
        });

        it('should add token to request', () => {
            service.getCurrentWeatherByCity(city);

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            expect(req).toBeDefined();

            const url = new URL('https:' + req?.request.url);
            expect(url.searchParams.get('appid')).toBe('xxx');
        });

        it('should add application locale to request', () => {
            service.getCurrentWeatherByCity(city);

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            expect(req).toBeDefined();

            const url = new URL('https:' + req?.request.url);
            expect(url.searchParams.get('lang')).toBe('aa');
        });

        it('should add city coordinates to request', () => {
            service.getCurrentWeatherByCity(city);

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            expect(req).toBeDefined();

            const url = new URL('https:' + req?.request.url);
            expect(url.searchParams.get('lat')).toBe('1');
            expect(url.searchParams.get('lon')).toBe('2');
        });

        it('should return requested weather', waitForAsync(async () => {
            // @ts-ignore
            const data: OnecallResponse = require('../../../test/mocks/open-weather-response-onecall.json');

            const expected: WeatherSummary = new OpenWeatherSummary(city, data.current);

            const actual$ = service.getCurrentWeatherByCity(city);

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            req.flush(data);

            const actual = await actual$;
            expect(actual).toEqual(expected);
        }));

        it('should throw a HTTP error for any error in HTTP request', waitForAsync(async () => {
            const actual$ = service.getCurrentWeatherByCity(city);

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            req.error('error' as unknown as ErrorEvent);

            try {
                await actual$;
                fail('Test should trow an error');
            } catch (e: any) {
                expect(e).toBeInstanceOf(HttpErrorResponse);
                expect(e.error).toEqual('error');
            }
        }));
    });

    describe('method getHourlyForecastByLocation', () => {
        const requestUrl = '//yyy/onecall';
        let city: City;

        beforeEach(() => {
            city = new City(100, 'foo', 1, 2, 'xxx');
        });

        it('should add token to request', () => {
            service.getHourlyForecastByCity(city);

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            expect(req).toBeDefined();

            const url = new URL('https:' + req?.request.url);
            expect(url.searchParams.get('appid')).toBe('xxx');
        });

        it('should add application locale to request', () => {
            service.getHourlyForecastByCity(city);

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            expect(req).toBeDefined();

            const url = new URL('https:' + req?.request.url);
            expect(url.searchParams.get('lang')).toBe('aa');
        });

        it('should add exclusions to request', () => {
            service.getHourlyForecastByCity(city);

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            expect(req).toBeDefined();

            const url = new URL('https:' + req?.request.url);
            expect(url.searchParams.get('exclude')).toBe('minutely,daily,alerts');
        });

        it('should add city coordinates to request', () => {
            service.getHourlyForecastByCity(city);

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            expect(req).toBeDefined();

            const url = new URL('https:' + req?.request.url);
            expect(url.searchParams.get('lat')).toBe('1');
            expect(url.searchParams.get('lon')).toBe('2');
        });

        it('should return requested forecast', waitForAsync(async () => {
            // @ts-ignore
            const data: OnecallResponse = require('../../../test/mocks/open-weather-response-onecall.json');

            const expected: WeatherSummary[] = [
                new OpenWeatherSummary(city, data.current),
                ...data.hourly.map((x: HourlyWeatherData)  => new OpenWeatherSummary(city, x))
            ];

            const actual$ = service.getHourlyForecastByCity(city);

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            req.flush(data);

            const actual = await actual$;
            expect(actual).toEqual(expected);
        }));

        it('should throw a HTTP error for any error in HTTP request', waitForAsync(async () => {
            const actual$ = service.getHourlyForecastByCity(city);

            const req = httpController.expectOne(x => x.url.startsWith(requestUrl));
            req.error('error' as unknown as ErrorEvent);

            try {
                await actual$;
                fail('Test should trow an error');
            } catch (e: any) {
                expect(e).toBeInstanceOf(HttpErrorResponse);
                expect(e.error).toEqual('error');
            }
        }));
    });
});
