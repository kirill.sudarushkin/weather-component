import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { WeatherWidgetModule } from '@backbase/weather-widget';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { GitlabSnippetComponent } from './gitlab-snippet.component';

const locales: { [locale: string]: () => Promise<any> } = {
    en: () => import('@angular/common/locales/en'),
    nl: () => import('@angular/common/locales/nl'),
    ru: () => import('@angular/common/locales/ru')
};

function getLocaleId(): string {
    const locale: string | null = location.pathname.substr(1, 2);
    const supportedLocales: string[] = Object.keys(locales);
    return locale && supportedLocales.includes(locale) ? locale : 'en';
}

function initializeLocale(): () => Promise<string> {
    const localeId = getLocaleId();

    return () => locales[localeId]?.().then(x => {
        registerLocaleData(x.default);
        return localeId;
    });
}

@NgModule({
    declarations: [
        AppComponent,
        GitlabSnippetComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        WeatherWidgetModule.forRoot({
            openWeatherApiToken: environment.openWeatherApiToken,
            refreshTimeout: environment.refreshTimeout
        })
    ],
    providers: [
        ...(environment.production ? [] : [
            {
                provide: APP_INITIALIZER,
                useFactory: initializeLocale,
                multi: true
            },
            {
                provide: LOCALE_ID,
                useFactory: getLocaleId
            }
        ])
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
