import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { WeatherSummary } from '../../models/weather-summary';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { WeatherWidgetConfig, weatherWidgetConfigToken } from '../../weather-widget.config';
import { WeatherResponse } from './api/weather.response';
import { City } from '../../models/city';
import { OnecallResponse } from './api/onecall.response';
import { WeatherProvider } from '../weather.provider';
import { CityNotFoundError } from '../../models/city-not-found.error';
import { OpenWeatherSummary } from './open-weather-summary';

const defaultApiPath: string = '//api.openweathermap.org/data/2.5';

export const apiTokenKey: string = 'appid';

const cityNotFoundErrorMessage = 'city not found';

function convertNgLocaleToApi(locale: string): string {
    return locale.split('-')[0];
}

type UrlWithLocale = { url: string, locale: string };

type UrlQueryParams = string[][] | Record<string, string> | string | URLSearchParams;

@Injectable()
export class OpenWeatherProvider extends WeatherProvider {

    constructor(
        private readonly http: HttpClient,
        @Inject(weatherWidgetConfigToken)
        private readonly config: WeatherWidgetConfig,
        @Inject(LOCALE_ID)
        private readonly locale: string
    ) {
        super();
    }

    public async searchCity(query: string): Promise<City> {
        const {url, locale} = this.buildRequestUrl('weather', {q: query});

        let data: WeatherResponse;

        try {
            data = await this.http.get(url).toPromise() as WeatherResponse;
        } catch (e: unknown) {
            if (e instanceof HttpErrorResponse && e.status === 404 && e.error?.message === cityNotFoundErrorMessage) {
                throw new CityNotFoundError(query);
            }

            throw e;
        }

        return new City(
            data.id,
            data.name,
            data.coord.lat,
            data.coord.lon,
            locale
        );
    }

    public async getCurrentWeatherByCity(city: City): Promise<WeatherSummary> {
        const {url} = this.buildRequestUrl('onecall', {
            lat: city.latitude.toString(10),
            lon: city.longitude.toString(10),
            exclude: 'minutely,hourly,daily,alerts'
        });

        const data: OnecallResponse = await this.http.get(url).toPromise() as OnecallResponse;

        if (!data.current) {
            throw new Error(`Couldn't get current forecast for the city "${city.name}"`);
        }

        return new OpenWeatherSummary(city, data.current);
    }

    public async getHourlyForecastByCity(city: City): Promise<WeatherSummary[]> {
        const {url} = this.buildRequestUrl('onecall', {
            lat: city.latitude.toString(10),
            lon: city.longitude.toString(10),
            exclude: 'minutely,daily,alerts'
        });

        const data: OnecallResponse = await this.http.get(url).toPromise() as OnecallResponse;

        if (!data.current || !data.hourly) {
            throw new Error(`Couldn't get hourly forecast for the city "${city.name}"`);
        }

        const forecast = data.hourly?.map(x => new OpenWeatherSummary(city, x));
        const current = new OpenWeatherSummary(city, data.current);
        forecast.unshift(current);

        return forecast;
    }

    private buildRequestUrl(path: string, params: UrlQueryParams): UrlWithLocale {
        const searchParams = new URLSearchParams(params);

        searchParams.append(apiTokenKey, this.config.openWeatherApiToken);

        const locale = this.locale;
        const apiLocale = convertNgLocaleToApi(locale);

        searchParams.append('lang', apiLocale);

        const apiPath: string = this.config.openWeatherApiUrl ?? defaultApiPath;
        const url: string = `${apiPath}/${path}?${searchParams.toString()}`;

        return {url, locale};
    }
}
