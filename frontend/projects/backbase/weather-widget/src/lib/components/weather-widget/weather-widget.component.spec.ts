import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { EMPTY } from 'rxjs';
import { cold, hot } from 'jest-marbles';

import { WeatherWidgetComponent } from './weather-widget.component';
import { WeatherWidgetStore } from '../../services/weather-widget.store';
import { CityNotFoundError } from '../../models/city-not-found.error';

describe('WeatherWidgetComponent', () => {
    let component: WeatherWidgetComponent;
    let fixture: ComponentFixture<WeatherWidgetComponent>;

    async function configure(error?: any): Promise<void> {
        await TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [WeatherWidgetComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .overrideComponent(WeatherWidgetComponent, {
                set: {
                    providers: [
                        {
                            provide: WeatherWidgetStore,
                            useValue: {
                                cities$: hot(
                                    '---^--x--y', {x: [], y: [{order: 2}, {order: 1}]}),
                                loading$: hot(
                                    '---^-xy-xy', {x: true, y: false}),
                                error$: error
                                    ? hot(
                                    '---^--x---y', {x: undefined, y: error})
                                    : EMPTY
                            }
                        }
                    ],
                }
            });

        fixture = TestBed.createComponent(WeatherWidgetComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }

    it('should create', async () => {
        await configure();
        expect(component).toBeTruthy();
    });

    it('forecasts$ should be ordered', async () => {
        await configure();
        expect(component.forecasts$).toBeObservable(cold(
            '---x--y', {x: [], y: [{order: 1}, {order: 2}]}
        ));
    });

    it('loading$ should be equals to store value', async () => {
        await configure();
        expect(component.loading$).toBeObservable(cold(
            '--xy-xy', {x: true, y: false}
        ));
    });

    it('error$ should emit only defined values', async () => {
        await configure('error');
        expect(component.error$).toBeObservable(cold(
            '-------x', {x: 'Unexpected error when loading weather data.'}
        ));
    });

    it('error$ should emit special message for CityNotFoundError', async () => {
        await configure(new CityNotFoundError('foo'));
        expect(component.error$).toBeObservable(cold(
            '-------x', {x: 'Couldn\'t find city for query "foo".'}
        ));
    });
});
