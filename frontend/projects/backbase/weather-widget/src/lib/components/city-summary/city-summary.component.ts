import { Component, ElementRef, Input, OnChanges, ViewChild } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { WeatherSummary } from '../../models/weather-summary';
import { SmartChanges } from '../../utils/change-detector.utils';
import { CityForecast, WeatherWidgetStore } from '../../services/weather-widget.store';

@Component({
    selector: 'lib-city-summary',
    templateUrl: './city-summary.component.html',
    styleUrls: ['./city-summary.component.scss'],
    animations: [
        trigger('slideInOut', [
            transition(':enter', [
                style({height: 0}),
                animate('200ms ease-in', style({height: '150px'}))
            ]),
            transition(':leave', [
                animate('200ms ease-in', style({height: 0}))
            ])
        ])
    ]
})
export class CitySummaryComponent implements OnChanges {

    @Input()
    public forecast?: CityForecast;

    @ViewChild('hourlyRef', {static: false})
    private hourlyRef?: ElementRef;

    public displayHourly: boolean = false;

    public current?: WeatherSummary;

    public hourly?: WeatherSummary[];

    constructor(
        private readonly store: WeatherWidgetStore
    ) {
    }

    public ngOnChanges(changes: SmartChanges<this>): void {
        if (changes.forecast) {
            const now: Date = new Date();
            this.current = this.forecast?.forecast
                ?.find(x => x.date <= now);

            this.hourly = this.forecast?.forecast
                ?.filter(x => x.date > now)
                .sort((a, b) => a.date.valueOf() - b.date.valueOf())
                .filter((x, i) => i < 24);
        }
    }

    public remove(): void {
        if (!this.forecast) {
            return;
        }

        this.store.removeCity(this.forecast.city);
    }

    public toggleHourly(): void {
        this.displayHourly = !this.displayHourly;
    }
}
