import { Pipe, PipeTransform } from '@angular/core';
import { Celsius, Kelvin } from '../models/number-guards';

@Pipe({
    name: 'kelvinToCelsius'
})
export class KelvinToCelsiusPipe implements PipeTransform {

    public transform(value: Kelvin): Celsius {
        return value - 273.15 as Celsius;
    }

}
